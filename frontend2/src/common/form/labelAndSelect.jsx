import React, { Component } from 'react'
import Grid from '../layout/grid'


export default class LabelAndSelect extends Component {

    renderOptions() {
        const values = [props.values]
        return values.map(v => (
            <option>{v}</option>
        ))
    }

    render() {
        return (

            <Grid cols={props.cols}>
                <div className='form-group'>
                    <label>{props.label}</label>
                    <select className='form-control' {...props.input}
                        readOnly={props.readOnly} type={props.type} ref="select">
                        {this.renderOptions()}
                    </select>
                </div>
            </Grid>
        )
    }
}

