import React from 'react'

export default props => (
    <footer className='main-footer'> 
        <strong> 
            Copyright &copy; 2019
            <a href='http://admGabinetes.com.br' target='_blank'> ViniCoder</a>.
        </strong>
    </footer>
)