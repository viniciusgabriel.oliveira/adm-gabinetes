import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, formValueSelector } from 'redux-form'

import { init } from './municipeActions'
import LabelAndInput from '../common/form/labelAndInput'
import LabelAndSelect from '../common/form/labelAndSelect'

class MunicipeForm extends Component {


    render() {
        const { handleSubmit, readOnly} = this.props

        return (
            <form role='form' onSubmit={handleSubmit}>
                <div className='box-body'>
                    <Field name='nome' component={LabelAndInput} readOnly={readOnly}
                        label='Nome' cols='12 6' placeholder='Informe o nome' />
                    <Field name='cep' component={LabelAndInput} type='number' readOnly={readOnly}
                        label='CEP' cols='12 2' placeholder='Informe o CEP' />
                    <Field name='numeroEnd' component={LabelAndInput} type='number' readOnly={readOnly}
                        label='Numero' cols='12 2' placeholder='Informe o Numero' />
                    <Field name='complementoEnd' component={LabelAndInput} readOnly={readOnly}
                        label='Complemento' cols='12 2' placeholder='Informe o Numero' />
                    <Field name='sexo' label='Sexo' cols='12 6' 
                        placeholder='Informe o Sexo' values={['Masculino','Feminino']} 
                        component={LabelAndSelect} readOnly={readOnly} />
                    

                    
                </div>
                <div className='box-footer'>
                    <button type='button' className='btn btn-primary'
                        onClick={this.props.init}>Incluir</button>
                    <button type='button' className='btn btn-default'
                        onClick={this.props.init}>Cancelar</button>
                </div>
            </form>
        )
    }
}

MunicipeForm = reduxForm({form: 'MunicipeForm', destroyOnUnmount: false})(MunicipeForm)
const selector = formValueSelector('MunicipeForm')
const mapStateToProps = state => ({
    credits: selector(state, 'credits'),
    debts: selector(state, 'debts')
})
const mapDispatchToProps = dispatch => bindActionCreators({init}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(MunicipeForm)