const Municipe = require('./municipes')
const errorHandler = require('../common/errorHandler')

Municipe.methods(['get', 'post', 'put', 'delete'])
Municipe.updateOptions({new: true, runValidators: true})
Municipe.after('post', errorHandler).after('put', errorHandler)



module.exports = Municipe