const restful = require('node-restful')
const mongoose = restful.mongoose

const municipeSchema = new mongoose.Schema({
    dataCadastro: {type: Date, default: Date.now},
    imprimir: String,
    devolvido: String,
    origemCadastro: String,
    nome: String,
    cpf: String,
    profissao: String,
    sexo: String,
    dataNascimento: Date,
    quemIndicou: String,
    escolaVotacao: String,
    zona: Number,
    secao: Number,
    tituloEleitor: Number,
    cepMunicipe: String,
    numeroEnd: String,
    complementoEnd: String,
    informacoes: String,
    observacoes: String
})

module.exports = restful.model('Municipe', municipeSchema)